package com.mts2dplot;


import java.util.ArrayList;


import android.graphics.Color;

import com.analysis.DataAnalysis;
import com.common.Complex;
import com.graph.DrawView;
import com.graph.Graph;
import com.graph.GraphLine;
import com.graph.LinePoint;
import com.mtfdata.MtData;
import com.mtfdata.MtManager;
import com.mtfdata.ZTensor;

public class MtsViewModel {
	
	private DrawView graph;
	
	public enum CalculationType{
		ApparentResistivityXXYY,
		ApparentResistivityXYYX,
		ApparentResistivityDet,
		ZAbsXXYY,
		ZAbsXYYX,
		RooXXYY,
		RooXYYX,
		RooDet,
		SwiftSkew,
		ZDet,
		TwoDim,
		SwiftStrike
	}
	
	public MtsViewModel(DrawView graph){
		this.graph = graph;
	}

	
	public void initializePlot(CalculationType type){
		switch(type){
			case ApparentResistivityXXYY:
				setLogNotation(true, true);
			break;
			case ApparentResistivityXYYX:
				setLogNotation(true, true);
			break;
			case ApparentResistivityDet:
				setLogNotation(true, true);
			break;
			case ZAbsXXYY:
				setLogNotation(true, true);
			break;
			case ZAbsXYYX:
				setLogNotation(true, true);
			break;
			case RooXXYY:
				setLogNotation(true, false);
			break;
			case RooXYYX:
				setLogNotation(true, false);
			break;
			case RooDet:
				setLogNotation(true, false);
			break;
			case ZDet:
				setLogNotation(true, true);
			break;
			case SwiftSkew:
				setLogNotation(true, false);
			break;
			case SwiftStrike:
				setLogNotation(true, false);
			break;
			default:
				setLogNotation(false, false);
			break;
		}
		
		calculate(type);
	}
	
	public void setLogNotation(boolean xFlag, boolean yFlag){
		graph.setLogarithmic(xFlag, yFlag);
	}
	
	
	private void calculate(CalculationType type){
		graph.clear();
		int n = 0;
		for(MtData data : MtManager.getInstance().getAllDatas()){
			
			ArrayList<LinePoint> yValues = new ArrayList<LinePoint>();
			ArrayList<LinePoint> yValues2 = new ArrayList<LinePoint>();
			
			ArrayList<Double> xValues = new ArrayList<Double>();
			
			for(int i=0; i<data.tensors.size(); i++){
				ZTensor tensor = data.tensors.get(i);
				switch(type){
					case ApparentResistivityXYYX:
						xValues.add(tensor.period);
						yValues.add(DataAnalysis.getAppResistivity(0,1,tensor));
						yValues2.add(DataAnalysis.getAppResistivity(1,0,tensor));
					break;
					case ApparentResistivityXXYY:
						xValues.add(tensor.period);
						yValues.add(DataAnalysis.getAppResistivity(0,0,tensor));
						yValues2.add(DataAnalysis.getAppResistivity(1,1,tensor));
					break;
					case ZAbsXXYY:
						xValues.add(tensor.period);
						yValues.add(DataAnalysis.getZAbs(0,0, tensor));
						yValues2.add(DataAnalysis.getZAbs(1,1, tensor));
					break;
					case ZAbsXYYX:
						xValues.add(tensor.period);
						yValues.add(DataAnalysis.getZAbs(0,1, tensor));
						yValues2.add(DataAnalysis.getZAbs(1,0, tensor));
					break;
					case ApparentResistivityDet:
						xValues.add(tensor.period);
						yValues.add(DataAnalysis.getAppResistivityDet(tensor));
					break;
					case ZDet:
						xValues.add(tensor.period);
						yValues.add(DataAnalysis.getZDet(tensor));
					break;
					case RooXXYY:
						xValues.add(tensor.period);
						yValues.add(DataAnalysis.getPhase(0,0,tensor));
						yValues2.add(DataAnalysis.getPhase(1,1, tensor));
					break;
					case RooXYYX:
						xValues.add(tensor.period);
						yValues.add(DataAnalysis.getPhase(0,1,tensor));
						yValues2.add(DataAnalysis.getPhase(1,0, tensor));
					break;
					case RooDet:
						xValues.add(tensor.period);
						yValues.add(new LinePoint(DataAnalysis.getPhase(tensor.determinant()),0));
					break;
					case SwiftSkew:
						xValues.add(tensor.period);
						yValues.add(DataAnalysis.getSwiftSkew(tensor));
					break;
					case SwiftStrike:
						if(i>0 && i<data.tensors.size()-1){
							xValues.add(tensor.period);
							yValues.add(DataAnalysis.getSwiftStrike(data.tensors.get(i-1), data.tensors.get(i+1)));
						}
					break;
					default:
						
					break;
				}
			}
			
			if(yValues.size()>0){
				GraphLine series1 = new GraphLine(
						xValues,
		                yValues,
		                GraphLine.LineType.Rect);
				series1.color = getColor(n);
				graph.addLine(series1);
			}
			
			if(yValues2.size()>0){
				GraphLine series2 = new GraphLine(
						xValues,
		                yValues2,
		                GraphLine.LineType.Circle);
				series2.color = getColor(n);
				graph.addLine(series2);
			}
			n++;
		}
		graph.invalidate();
	}
	
	private int getColor(int n){
		int list[] = new int[]{Color.BLUE, Color.RED, Color.YELLOW, Color.MAGENTA, Color.GREEN, Color.CYAN}; 
		return list[n%6];
	}
	
}
