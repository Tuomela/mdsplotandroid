package com.mts2dplot;

import java.util.List;

import com.mds2dplot.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class FunctionAdapter extends ArrayAdapter<FunctionOption> {

	private Context c;
    private int id;
    private List<FunctionOption>items;
	
	public FunctionAdapter(Context context, int resource, List<FunctionOption> items) {
		super(context, resource, items);
		// TODO Auto-generated constructor stub
		c = context;
		id = resource;
		this.items = items;
	}
	
	public FunctionOption getItem(int i)
	{
	    return items.get(i);
	}
    
    public List<FunctionOption> getItems(){
    	return items;
    }
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(id, null);
        }
        final FunctionOption o = items.get(position);
        if (o != null) {
            TextView t1 = (TextView) v.findViewById(R.id.function_title);
            t1.setText(o.title);
        }
        return v;
	}

}
