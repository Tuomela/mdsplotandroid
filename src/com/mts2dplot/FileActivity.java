package com.mts2dplot;

import com.mds2dplot.R;
import com.mtfdata.MtManager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class FileActivity extends Activity {
    
    private File currentDir;
    private ListView list;
    private FileArrayAdapter adapter;
    
    @SuppressLint("SdCardPath") @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);
        
        list = (ListView)findViewById(R.id.listView);
        
        currentDir = new File("/sdcard/");
        fill(currentDir);
    }
    private void fill(File f)
    {
        File[]dirs = f.listFiles();
         this.setTitle("Current Dir: "+f.getName());
         List<Option>dir = new ArrayList<Option>();
         List<Option>fls = new ArrayList<Option>();
         try{
             for(File ff: dirs)
             {
                if(ff.isDirectory())
                    dir.add(new Option(ff.getName(),"Folder",ff.getAbsolutePath()));
                else
                {
                	if(ff.getName().endsWith(".mtf")){
                		fls.add(new Option(ff.getName(),"File Size: "+ff.length(),ff.getAbsolutePath()));
                	}
                }
             }
         }catch(Exception e)
         {
             
         }
         Collections.sort(dir);
         Collections.sort(fls);
         dir.addAll(fls);
         if(!f.getName().equalsIgnoreCase("sdcard"))
             dir.add(0,new Option("..","Parent Directory",f.getParent()));
         
         adapter = new FileArrayAdapter(this,R.layout.file_view,dir);
         list.setAdapter(adapter);
         
         list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long arg3) {
				// TODO Auto-generated method stub
				Option o = adapter.getItem(position);
	     		if(o.getData().equalsIgnoreCase("folder")||o.getData().equalsIgnoreCase("parent directory")){
	     				currentDir = new File(o.getPath());
	     				fill(currentDir);
	     		}else{
	     			o.chosen = !o.chosen;
	     		}
	     		adapter.notifyDataSetChanged();
			}
         });
    }
    
    public void okClick(View view){
    	for(Option o : adapter.getItems()){
    		if(o.getChosen()){
    			MtManager.getInstance().loadMtsFileToManager(o.getPath());
    		}
    	}
    	finish();
    }
    
    public void cancelClick(View view){
    	finish();
    }
}
