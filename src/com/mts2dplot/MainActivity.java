package com.mts2dplot;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.graph.DrawView;
import com.mds2dplot.R;
import com.mts2dplot.MtsViewModel.CalculationType;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class MainActivity extends ActionBarActivity {

	private static final String _apparentResistivityXXYY = "Roo XX YY";
	private static final String _apparentResistivityDet = "Roo Det";
	private static final String _apparentResistivityXYYX = "Roo XY YX";
	private static final String _zAbsXXYY = "ZAbs XX YY";
	private static final String _zAbsXYYX = "ZAbs XY YX";
	private static final String _rooXXYY = "Phase XX YY";
	private static final String _rooXYYX = "Phase XY YX";
	private static final String _rooDet = "Phase Det";
	private static final String _zDet = "det Z avg";
	private static final String _swiftSkew = "Swift Skew";
	private static final String _swiftStrike = "Swift Strike";
	
	private MtsViewModel _mdsHandler;
	private ListView functionList;
	private FunctionAdapter functionAdapter;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        
        setContentView(R.layout.activity_main);
        
        // initialize our view model reference:
        DrawView graph = (DrawView) findViewById(R.id.graph);
        _mdsHandler = new MtsViewModel(graph);
        functionList = (ListView) findViewById(R.id.list);
        makeFunctionList();
    }
    
    private void makeFunctionList(){
    	
    	List<FunctionOption> functions = new ArrayList<FunctionOption>();
    	functions.add(new FunctionOption(_apparentResistivityXXYY));
    	functions.add(new FunctionOption(_apparentResistivityXYYX));
    	functions.add(new FunctionOption(_apparentResistivityDet));
    	functions.add(new FunctionOption(_zAbsXXYY));
    	functions.add(new FunctionOption(_zAbsXYYX));
    	functions.add(new FunctionOption(_zDet));
    	functions.add(new FunctionOption(_rooXXYY));
    	functions.add(new FunctionOption(_rooXYYX));
    	functions.add(new FunctionOption(_rooDet));
    	functions.add(new FunctionOption(_swiftSkew));
    	functions.add(new FunctionOption(_swiftStrike));
    	
    	functionAdapter = new FunctionAdapter(this, R.layout.function_view, functions);
    	functionList.setAdapter(functionAdapter);
        
    	functionList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long arg3) {
				FunctionOption o = functionAdapter.getItem(position);
				String funcName = o.title;
				
				if(funcName.equals(_apparentResistivityXXYY)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.ApparentResistivityXXYY);
				}else if(funcName.equals(_apparentResistivityXYYX)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.ApparentResistivityXYYX);
				}else if(funcName.equals(_apparentResistivityDet)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.ApparentResistivityDet);
				}else if(funcName.equals(_zAbsXXYY)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.ZAbsXXYY);
				}else if(funcName.equals(_zAbsXYYX)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.ZAbsXYYX);
				}else if(funcName.equals(_zDet)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.ZDet);
				}else if(funcName.equals(_rooXXYY)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.RooXXYY);
				}else if(funcName.equals(_rooXYYX)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.RooXYYX);
				}else if(funcName.equals(_rooDet)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.RooDet);
				}else if(funcName.equals(_swiftSkew)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.SwiftSkew);
				}else if(funcName.equals(_swiftStrike)){
					_mdsHandler.initializePlot(MtsViewModel.CalculationType.SwiftStrike);
				}
				
	     		functionAdapter.notifyDataSetChanged();
			}
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will 
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_open){
        	Intent intent = new Intent(this, FileActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
    
}
