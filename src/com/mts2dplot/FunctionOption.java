package com.mts2dplot;

public class FunctionOption implements Comparable<FunctionOption>{

	String title;
	
	public FunctionOption(String name){
		title = name;
	}
	
	@Override
	public int compareTo(FunctionOption b) {	
		return this.title.toLowerCase().compareTo(b.title.toLowerCase());
	}
	
}
