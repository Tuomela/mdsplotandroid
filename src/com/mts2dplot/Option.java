package com.mts2dplot;


public class Option implements Comparable<Option>{
    private String name;
    private String data;
    private String path;
    
    public boolean chosen;
    
    public Option(String n,String d,String p)
    {
        name = n;
        data = d;
        path = p;
        chosen = false;
    }
    public String getName()
    {
        return name;
    }
    public String getData()
    {
        return data;
    }
    public String getPath()
    {
        return path;
    }
    public boolean getChosen(){
    	if(!name.equalsIgnoreCase("folder") && !name.equalsIgnoreCase("parent folder")){
    		return chosen;
    	}else{
    		return false;
    	}
    }
    @Override
    public int compareTo(Option o) {
        if(this.name != null)
            return this.name.toLowerCase().compareTo(o.getName().toLowerCase()); 
        else 
            throw new IllegalArgumentException();
    }
}


