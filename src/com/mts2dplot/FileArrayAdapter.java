package com.mts2dplot;


import java.util.List;

import com.mds2dplot.R;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


public class FileArrayAdapter extends ArrayAdapter<Option>{

    private Context c;
    private int id;
    private List<Option>items;
    
    public FileArrayAdapter(Context context, int textViewResourceId,
            List<Option> objects) {
        super(context, textViewResourceId, objects);
        c = context;
        id = textViewResourceId;
        items = objects;
    }
    public Option getItem(int i)
	{
	    return items.get(i);
	}
    
    public List<Option> getItems(){
    	return items;
    }
    
     @Override
    public View getView(int position, View convertView, ViewGroup parent) {
           View v = convertView;
           if (v == null) {
               LayoutInflater vi = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
               v = vi.inflate(id, null);
           }
           final Option o = items.get(position);
           if (o != null) {
               TextView t1 = (TextView) v.findViewById(R.id.TextView01);
               TextView t2 = (TextView) v.findViewById(R.id.TextView02);
               LinearLayout layout = (LinearLayout) v.findViewById(R.id.fileItemLayout);
               
               if(t1!=null)
                   t1.setText(o.getName());
               if(t2!=null)
                   t2.setText(o.getData());
                   
               if(!o.getData().equalsIgnoreCase("folder") && !o.getData().equalsIgnoreCase("parent directory")){
            	   if(o.getChosen()){
            		   	layout.setBackgroundColor(Color.GRAY);
	        	   }else{
	        		   layout.setBackgroundColor(Color.WHITE);
	        	   }
               }else{
            	   layout.setBackgroundColor(Color.WHITE);
               }
                   
         }
         return v;
    }

}

