package com.graph;

public class GraphHelper {
	
	public static double getMinX(double minX, double maxX){
		int step = getStep(minX, maxX);
		int result = (int)minX+step*2;
		result -= result%step;
		while(result>minX){
			result -= step;
		}
		return result;
	}
	
	public static double getMaxX(double minX, double maxX){
		int step = getStep(minX, maxX);
		int result = (int)maxX-step;
		result -= result%step;
		while(result<maxX){
			result += step;
		}
		return result;
	}
	
	private static int getStep(double minX, double maxX){
		if(maxX-minX>120){
			return 90;
		}else if(maxX-minX>60){
			return 30;
		}else if(maxX-minX>20){
			return 10;
		}else{
			return 1;
		}
	}
	
}
