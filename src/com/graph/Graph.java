package com.graph;

import com.mds2dplot.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Graph extends ViewGroup {

	public DrawView drawArea;
	
	public Graph(Context context) {
		super(context);
		
		initView(context);
	}
	
	public Graph(Context context, AttributeSet attrs){
		super(context, attrs);
		
		initView(context);
	}
	
	public Graph(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs);
		
		initView(context);
	}
	
	private void initView(Context context){
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		addView(inflater.inflate(R.layout.graph_view, null));
		//drawArea = (DrawView)findViewById(R.id.drawArea);
	}

	@Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        // TODO Auto-generated method stub
        for(int i = 0 ; i < getChildCount() ; i++){
            getChildAt(i).layout(l, t, r, b);
        }
    }

}
