package com.graph;

public class LinePoint{
	
	public double value;
	public double error;
	
	public LinePoint(double v, double err){
		value = v;
		error = err;
	}
}
