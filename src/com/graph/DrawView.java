package com.graph;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class DrawView extends View {

	Paint paint = new Paint();
	private List<GraphLine> lines;
	
	private double minX, maxX;
	private double minY, maxY;
	
	private double width, height;
	private double drawableW, drawableH;
	private double marginLeft = 70, marginBottom = 50;
	private double marginRight = 10, marginTop = 10;
	
	private boolean logX = false, logY = false;

    public DrawView(Context context) {
        super(context);
        init();
    }
    
    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    
    public void setLogarithmic(boolean xFlag, boolean yFlag){
    	logX = xFlag;
    	logY = yFlag;
    }
    
    private void init(){
    	paint.setColor(Color.BLACK);
        lines = new ArrayList<GraphLine>();
        width = getWidth();
        height = getHeight();
        drawableW = width-marginLeft;
        drawableH = height-marginBottom;
        //Log.v("DrawView", width+", "+height+". drawable area: "+drawableW+", "+drawableH);
    }
    
    @Override
    public void onSizeChanged (int w, int h, int oldw, int oldh){
    	width = w;
    	height = h;
    	drawableW = width-marginLeft-marginRight;
        drawableH = height-marginBottom-marginTop;
    	
    	//Log.v("DrawView", "width and height = "+ w+", "+h);
    }
    
    private void setAxels(){
    	if(lines.size()>0){
	    	double nX = lines.get(0).minX;
	    	double nY = lines.get(0).minY;
	    	double pX = lines.get(0).maxX;
	    	double pY = lines.get(0).maxY;
	    	for(GraphLine line : lines){
	    		if(line.minX<nX) nX = line.minX;
	    		if(line.maxX>pX) pX = line.maxX;
	    		if(line.minY<nY) nY = line.minY;
	    		if(line.maxY>pY) pY = line.maxY;
	    	}
	    	
	    	if(logX){
	    		minX = GraphHelper.getMinX(Math.log10(nX), Math.log10(pX));
	    		maxX = GraphHelper.getMaxX(Math.log10(nX), Math.log10(pX));
	    	}else{
	    		minX = GraphHelper.getMinX(nX, pX);
	    		maxX = GraphHelper.getMaxX(nX, pX);
	    	}
	    	
	    	if(logY){
	    		minY = GraphHelper.getMinX(Math.log10(nY), Math.log10(pY));
    			maxY = GraphHelper.getMaxX(Math.log10(nY), Math.log10(pY));
	    	}else{
	    		minY = GraphHelper.getMinX(nY, pY);
    			maxY = GraphHelper.getMaxX(nY, pY);
	    	}
    	}
    	//Log.v("DrawView: ","min and max X = "+minX+", "+maxX);
    }

    
    public void addLine(GraphLine line){
    	lines.add(line);
    }
    
    public void clear(){
    	lines.clear();
    }
    
    private double getXPos(double x){
    	if(logX) x = Math.log10(x);
    	
    	double xW = maxX-minX;
    	if(xW <= 0) xW = 1;
    	
    	double res = (x-minX)*(drawableW/xW) + marginLeft;
    	
    	//Log.v("DrawView: ", "got x position "+ res + "with xW "+ xW);
    	
		return res;
    }
    
    private double getYPos(double y){
    	if(logY) y = Math.log10(y);
    	
    	double yW = maxY-minY;
    	if(yW <= 0) yW = 1;
    	
    	double res = marginTop + drawableH - (y-minY)*(drawableH/yW);
    	
    	//Log.v("DrawView: ", "got y position "+ res + "with yW "+ yW);
    	
		return res;
    }
    
    private String getXPosText(double i){
    	
    	Locale locale  = new Locale("en", "UK");
        String pattern = "###.###";

        DecimalFormat decimalFormat = (DecimalFormat)
                NumberFormat.getNumberInstance(locale);
        decimalFormat.applyPattern(pattern);
        
        return decimalFormat.format(i);
    }
    
    private String getYPosText(double j){
    	
    	Locale locale  = new Locale("en", "UK");
        String pattern = "###.##";

        DecimalFormat decimalFormat = (DecimalFormat)
                NumberFormat.getNumberInstance(locale);
        decimalFormat.applyPattern(pattern);
    	
    	return decimalFormat.format(j);
    }
    

    @Override
    public void onDraw(Canvas canvas) {
    	
    	if(lines.size()>0){
    		
    		paint.setTextSize(12);
	    	setAxels();
	        for(GraphLine line : lines){
	        	
	        	paint.setColor(line.color);
	        	
	        	for(int n=0; n<line.xValues.size(); n++){
	        		double x = line.xValues.get(n);
	        		double y = line.yValues.get(n).value;
	        		
	        		//DRAW POINT
	        		if(line.type==GraphLine.LineType.Rect){
	        			canvas.drawRect((float)getXPos(x)-2, (float)getYPos(y)-2, (float)getXPos(x)+2, (float)getYPos(y)+2, paint);
	        		}else if(line.type==GraphLine.LineType.Circle){
	        			canvas.drawCircle((float)getXPos(x), (float)getYPos(y), 4, paint);
	        		}
	        		
	        		//DRAW ERROR LINES
	        		double errH = getYPos(y-line.yValues.get(n).error)-getYPos(y+line.yValues.get(n).error);
	        		if(errH>1){
	        			canvas.drawLine((float)getXPos(x), (float)getYPos(y)-(float)errH/2, (float)getXPos(x), (float)getYPos(y)+(float)errH/2, paint);
	        			canvas.drawLine((float)getXPos(x)-2, (float)getYPos(y)+(float)errH/2, (float)getXPos(x)+2, (float)getYPos(y)+(float)errH/2, paint);
	        			canvas.drawLine((float)getXPos(x)-2, (float)getYPos(y)-(float)errH/2, (float)getXPos(x)+2, (float)getYPos(y)-(float)errH/2, paint);
	        		}
	        	}
	        }
	        
	        paint.setColor(Color.BLACK);
	        //canvas.drawLine((float)marginLeft, 0,(float)marginLeft,(float)drawableH, paint);
	        //canvas.drawLine((float)marginLeft, (float)drawableH,(float)width,(float)drawableH, paint);
	        
	        
	        double stepX = 1;
	        double stepY = 0.1;
	        
	        if(maxY-minY>120){
	        	stepY = 90;
	        }else if(maxY-minY>60){
	        	stepY = 30;
	        }else if(maxY-minY>20){
	        	stepY = 10;
	        }else if(maxY-minY>2){
	        	stepY = 1;
	        }
	        
	        paint.setTextSize(20);
	        
	        if(logX){
	        	// Y suuntaiset suorat logaritmiselle asteikolle
		        for(double i=minX; i<=maxX; i += stepX){
		        	 canvas.drawLine((float)getXPos(Math.pow(10,i)), 0,(float)getXPos(Math.pow(10,i)),(float)(drawableH+marginTop), paint);
		        	 if(i<maxX) canvas.drawText(getXPosText(Math.pow(10,i)), (float)getXPos(Math.pow(10,i))-10, (float)drawableH+30, paint);
		        }     
	        }else{
	        	// Y suuntaiset suorat perus asteikolle
	        	for(double i=minX; i<=maxX; i += stepX){
		        	 canvas.drawLine((float)getXPos(i), 0,(float)getXPos(i),(float)(drawableH+marginTop), paint);
		        	 if(i<maxX) canvas.drawText(getXPosText(i), (float)getXPos(i)-10, (float)drawableH+30, paint);
		        }       	
	        }
	        
	        if(logY){
	        	//X suuntaiset suorat logaritmiselle asteikolle
		        for(double j=minY; j<=maxY; j += stepY){
		       	 	canvas.drawLine((float)marginLeft, (float)getYPos(Math.pow(10,j)),(float)width,(float)getYPos(Math.pow(10,j)), paint);
		       	 	if(j<=maxY) canvas.drawText(getYPosText(Math.pow(10,j)), 0, (float)getYPos(Math.pow(10,j))+10, paint);
		        }
	        }else{
	        	//X suuntaiset suorat perus asteikolle
	        	for(double j=minY; j<=maxY; j += stepY){
		       	 	canvas.drawLine((float)marginLeft, (float)getYPos(j),(float)width,(float)getYPos(j), paint);
		       	 	if(j<=maxY) canvas.drawText(getYPosText(j), 0, (float)getYPos(j)+10, paint);
		        }
	        }
    	}else{
    		
    		paint.setColor(Color.BLACK);
    		paint.setTextSize(48);
    		canvas.drawText("welcome", (float)width/2-100, (float)height/2-5, paint);
    		
    	}
    }

}
