package com.graph;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;

public class GraphLine {
	
	public enum LineType{
		Rect,
		Circle
	}
	
	public List<Double> xValues;
	public List<LinePoint> yValues;
	public LineType type;
	
	public int color;
	
	public double maxY = -999999, maxX = -999999;
	public double minY = 999999, minX = 999999;
	
	public GraphLine(){
		xValues = new ArrayList<Double>();
		yValues = new ArrayList<LinePoint>();
		type = LineType.Circle;
	}
	
	public GraphLine(List<Double> xVal, List<LinePoint> yVal, LineType type){
		xValues = xVal;
		yValues = yVal;
		checkRange();
		this.type = type;
	}
	
	public void setPoint(double x, LinePoint y){
		xValues.add(x);
		yValues.add(y);
		if(x>maxX) maxX = x;
		if(x<minX) minX = x;
		if(y.value>maxY) maxY = y.value;
		if(y.value<minY) minY = y.value;
	}
	
	private void checkRange(){
		for(double x : xValues){
			if(x>maxX) maxX = x;
			if(x<minX) minX = x;
		}
		for(LinePoint y : yValues){
			if(y.value>maxY) maxY = y.value;
			if(y.value<minY) minY = y.value;
		}
	}
	
}
