package com.analysis;

import com.common.Complex;
import com.graph.LinePoint;
import com.mtfdata.ZTensor;

public class DataAnalysis {
	public static LinePoint getZDet(ZTensor tensor){
		return new LinePoint(Math.sqrt(tensor.determinant().abs()), 0);
	}
	
	public static LinePoint getAppResistivityDet(ZTensor tensor){
		return new LinePoint(getAppResistivity(Math.sqrt(tensor.determinant().abs()), tensor.period), 0);
	}
	
	public static LinePoint getAppResistivity(int x, int y, ZTensor tensor){
		return new LinePoint(getAppResistivity(tensor.get(x,y).abs(), tensor.period), getAppResistivityErr(tensor.get(x,y).abs(), tensor.period, tensor.getError(x,y), tensor.period*0.001));
	}
	
	public static LinePoint getZAbs(int x, int y, ZTensor tensor){
		return new LinePoint(tensor.get(x,y).abs(), 0);
	}
	
	public static double getAppResistivity(double Z, double T){
		return 0.2*(Math.pow(Z, 2)*T);
	}
	
	public static double getAppResistivityErr(double Z, double T, double zErr, double tErr){
		return Math.abs(0.4*Z*T*zErr)+Math.abs(0.2*Z*Z*tErr);
	}
	
	public static LinePoint getPhase(int x, int y, ZTensor tensor){
		return new LinePoint(getPhase(tensor.get(x,y)), 0);
	}
	
	public static double getPhase(Complex complex){
		return Math.atan2(complex.img , complex.re)/Math.PI*180;
	}
	
	public static LinePoint getSwiftSkew(ZTensor tensor){
		return new LinePoint(tensor.get(0,0).add(tensor.get(1,1)).abs()/tensor.get(0,1).sub(tensor.get(1,0)).abs(),0);
	}
	
	public static LinePoint getSwiftStrike(ZTensor prevTensor, ZTensor nextTensor){
		double dPeriod = nextTensor.period-prevTensor.period;
		Complex S2 = (nextTensor.get(0,1).add(nextTensor.get(1,0)).sub(prevTensor.get(0,1).add(prevTensor.get(1,0))));
		Complex D1 = (nextTensor.get(0,0).sub(nextTensor.get(1,1)).sub(prevTensor.get(0,0).sub(prevTensor.get(1,1))));
		Complex dS2 = S2.mult(1.0/dPeriod);
		Complex dD1 = D1.mult(1.0/dPeriod);
		double angleRad = Math.atan(2*(dS2.mult(dD1).re)/(Math.pow(dD1.abs(),2))-Math.pow(dS2.abs(),2))/4;
		return new LinePoint(angleRad/Math.PI*180, 0);
	}
}
