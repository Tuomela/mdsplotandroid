package com.common;

public class Complex {
	
	public double re, img;
	
	public Complex(){
		re = 0;
		img = 0;
	}
	
	public Complex(double r, double i){
		re = r;
		img = i;
	}
	
	public Complex add(Complex b){
		return new Complex(re+b.re, img+b.img);
	}
	
	public Complex sub(Complex b){
		return new Complex(re-b.re, img-b.img);
	}
	
	public Complex mult(Complex b){
		return new Complex(re * b.re - img * b.img, re * b.img + img * b.re);
	}
	
	public Complex mult(double f){
		return new Complex(re * f, img * f);
	}
	
	public double abs(){
		return Math.sqrt(re*re + img*img);
	}
	
}
