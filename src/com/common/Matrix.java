package com.common;

public class Matrix {
	
	public Complex values[][];
	
	public Matrix(){
		values = new Complex[2][2];
	}
	
	public void set(int i, int j, Complex value){
		if(i>=0 && i<2 && j>=0 && j<2){
			values[i][j] = value;
		}
	}
	
	public Complex get(int i, int j){
		if(i>=0 && i<2 && j>=0 && j<2){
			return values[i][j];
		}else{
			return null;
		}
	}
	
	public Complex determinant(){
		//     [0,0] * [1,1] - [0,1] * [1,0]
		return get(0,0).mult(get(1,1)).sub(get(0,1).mult(get(1,0)));
	}
	
	public Matrix mult(Matrix b){
		Matrix result = new Matrix();
		
		for(int n=0; n<2; n++){
			for(int m=0; m<2; m++){
				Complex value = new Complex();
				for(int k=0; k<2; k++){
					value.add(get(k, m).mult(b.get(n, k)));
				}
				result.set(n, m, value);
			}
		}
		
		return result;
	}
	
}
