package com.mtfdata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.os.Environment;
import android.util.Log;

import com.common.Complex;

public class MtManager {
	
	private static MtManager _instance;
	
	public static MtManager getInstance(){
		if(_instance == null){
			_instance = new MtManager();
		}
		return _instance;
	}
	
	private List<MtData> _datas;
	
	public MtManager(){
		_datas = new ArrayList<MtData>();
	}
	
	public MtData get(int n){
		if(n>=0 && n<_datas.size()){
			return _datas.get(n);
		}else{
			return null;
		}
	}
	
	public List<MtData> getAllDatas(){
		return _datas;
	}
	
	@SuppressWarnings("resource")
	public void loadMtsFileToManager(String filename){
		
		if(filename.startsWith("/sdcard")){
			filename = filename.substring(7);
		}
		
		//check if this file has loaded already
		boolean hit = false;
		for(MtData temp : _datas){
			if(temp.filename.equals(filename)){
				hit = true;
			}
		}
		if(!hit) {
			MtData data = new MtData();
			data.filename = filename;
			
			File sdcard = Environment.getExternalStorageDirectory();
	
			//Get the text file
			File file = new File(sdcard, filename);
	
			//Read text from file
			StringBuilder text = new StringBuilder();
	
			try {
			    BufferedReader br = new BufferedReader(new FileReader(file));
			    String line;
			    
			    boolean readingImpedancePoints = false;
	
			    while ((line = br.readLine()) != null) {
			        text.append(line);
			        text.append('\n');
			        
			        //Start to read impedance tensor
			        if(line.startsWith("//SECTION=IMP")){
			        	readingImpedancePoints = true;
			        }else if(readingImpedancePoints){
			        	if(line.startsWith(" ")){
			        		line = line.substring(1);
			        	}
			        	String[] list = line.split(" +");
			        	
			        	//valid impedance tensor
			        	if(list.length == 14){
			        		
			        		ZTensor tensor = new ZTensor();
			        		tensor.period = Float.parseFloat(list[0]);
			        		
			        		double allAbs = 0;
			        		
			        		//n tells list's index, and v descripes value's index
			        		for(int n=2, v=0; n+2<14; n += 3, v++){
			        			Complex value = new Complex(Float.parseFloat(list[n]), Float.parseFloat(list[n+1]));
			        			double error = Float.parseFloat(list[n+2]);
			        			int i = v%2;
			        			int j = (v/2)%2;
			        			allAbs += value.abs();
			        			//Log.v("MtfFileReader:",i+", "+j+", ("+value.re+", "+value.img+")");
			        			tensor.set(i, j, value);
			        			tensor.setError(i, j, error);
			        		}
			        		
			        		if(allAbs<1000000000){
			        			data.addTensor(tensor);
			        		}
			        	}
			        }
			        
			        
			    }	    
			}
			catch (IOException e) {
				Log.v("MtfFileReader", "Error when reading file: "+filename);
			}
			
			_datas.add(data);
		}
	}
	
}
