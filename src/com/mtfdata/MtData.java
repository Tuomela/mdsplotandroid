package com.mtfdata;

import java.util.ArrayList;
import java.util.List;


public class MtData {
	
	public List<ZTensor> tensors;
	public String filename;
	
	public MtData(){
		tensors = new ArrayList<ZTensor>();
	}
	
	public void addTensor(ZTensor tensor){
		tensors.add(tensor);
	}
	
}
