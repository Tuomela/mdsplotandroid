package com.mtfdata;

import com.common.Matrix;

public class ZTensor extends Matrix {
	
	public double[][] errors;
	
	public double period;
	
	public ZTensor(){
		super();
		errors = new double[2][2];
	}
	
	public void setError(int i, int j, double value){
		if(i>=0 && i<2 && j>=0 && j<2){
			errors[i][j] = value;
		}
	}
	
	public double getError(int i, int j){
		if(i>=0 && i<2 && j>=0 && j<2){
			return errors[i][j];
		}else{
			return 0;
		}
	}
	
}
